import { configureStore } from '@reduxjs/toolkit'
import PersonalInfoReducer from './personalInfo'
import accordionReducer from './accordion'
import petInfoReducer from './petInfo'

// import CounterReducer from './counter';
export const store = configureStore({
  reducer: {
      personalInfo:PersonalInfoReducer,
      accordion:accordionReducer,
      petInfo:petInfoReducer,
  },
})