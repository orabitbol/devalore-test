import { createSlice } from '@reduxjs/toolkit'



const initialState = {
  name: '',
  age: 0,
}

export const PersonalInfo = createSlice({
  name: 'PersonalInfo',
  initialState,
  reducers: {
    getName: (state, action) => {
        state.name = action.payload;
    },
    getAge: (state, action) => {
        state.age = action.payload;
    },
  },
})

// Action creators are generated for each case reducer function
export const { getName, getAge } = PersonalInfo.actions

export default PersonalInfo.reducer