import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    personalInfo: true,
    petInfo: false,
    agreement: false
}

export const Accordion = createSlice({
    name: 'accordion',
    initialState,
    reducers: {
        next: (state) => {
            if (state.personalInfo == true) {
                state.petInfo = true;
                state.agreement = false;
                state.personalInfo = false;
            }
            else if (state.petInfo == true) {
                state.agreement = true;
                state.personalInfo = false;
                state.petInfo = false;
            }
   
        },
    },
})

export const { next } = Accordion.actions

export default Accordion.reducer