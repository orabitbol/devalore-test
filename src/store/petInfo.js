import { createSlice } from '@reduxjs/toolkit'




const initialState = {
    name: '',
    type: 'DOG',
}


export const petInfo = createSlice({
    name: 'petInfo',
    initialState,
    reducers: {
        petName: (state, action) => {
            state.name = action.payload;
        },
        setType: (state, action) => {
            state.type = action.payload;
        }

    },
})

// Action creators are generated for each case reducer function
export const { petName, setType } = petInfo.actions

export default petInfo.reducer