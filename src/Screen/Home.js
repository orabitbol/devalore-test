import React,{useState} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import accordion from '../store/accordion';
import {next} from '../store/accordion'
import { getName, getAge } from '../store/personalInfo'
// import { getName, getAge } from './store/PersonalInfo';

const Home = () => {
    
    const [name,setName] = useState('');
    const [age,setAge] = useState();

const dispatch = useDispatch();    


const getPersonalInfo = () =>{
    dispatch(getName(name));
    dispatch(getAge(age));
    dispatch(next());
}




    return(
        <div className='flex flex-col items-center p-3 '>
            <input 
            className=''
            onChange={(e) => setName(e.target.value)} 
            value = {name} 
            placeholder='Enter Your Name'
            maxLength='24'
            />
            

            <input 
            onChange={(e) => setAge(e.target.value)} 
            value = {age}  
            placeholder='Enter Your Age'  />

            <button  onClick={ getPersonalInfo }>
                Click me
            </button>

        </div>
    )
}
export default Home;