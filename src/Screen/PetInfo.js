import React,{useState} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {next} from '../store/accordion'
import { petName, setType } from '../store/petInfo'


const PetInfo = () => {
    
    const [Name,setName] = useState('');
    
    



const dispatch = useDispatch();



const getPersonalInfo = () =>{
    dispatch(petName(Name));
   
    dispatch(next());
}


    return(
        <div className='flex flex-col'>
            <input 
            onChange={(e) => setName(e.target.value)} 
            value = {Name} 
            placeholder="Enter Your Pet's Name" />

            <select onChange={(e) => setType(e.target.value)}>
                <option value={'DOG'}>Dog</option>
                <option value={'CAT'}>Cat</option>
                <option value={'HORSE'}>Horse</option>
                <option value={'OTHER'}>Other</option>
            </select>

            <button  onClick= {getPersonalInfo}>
                Click me
            </button>

        </div>
    )
}
export default PetInfo;