import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import personalInfo from '../store/personalInfo';


const Agreement = () => {


    const [checked, setChecked] = useState(false);

    const personalName = useSelector((state) => state.personalInfo.name);
    const personalAge = useSelector((state) => state.personalInfo.age);

    const petName = useSelector((state) => state.petInfo.name);
    const petType = useSelector((state) => state.petInfo.type);



    const alertData = () => {
        alert(personalName + ' ' + personalAge + ' ' + petName + ' ' + petType);
    }

    const handleChange = () => {
        setChecked(!checked);
    }


    return (
        <div className='flex flex-col'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget mi proin sed libero enim sed faucibus turpis. Convallis aenean et tortor at risus. Arcu cursus vitae congue mauris rhoncus aenean vel. Donec ultrices tincidunt arcu non sodales. Egestas maecenas pharetra convallis posuere morbi. Accumsan tortor posuere ac ut consequat. Diam quam nulla porttitor massa id neque aliquam. Felis imperdiet proin fermentum leo vel orci porta non pulvinar. Massa tempor nec feugiat nisl pretium. Cras semper auctor neque vitae tempus quam pellentesque nec nam. Lacus sed viverra tellus in hac. Commodo quis imperdiet massa tincidunt. At imperdiet dui accumsan sit amet nulla facilisi. Odio ut enim blandit volutpat maecenas volutpat. Faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing. Eget nullam non nisi est sit amet facilisis magna etiam.
            <br/>
            <br/>
            <br/>
            <label>
            <input type="checkbox" checked={checked} onChange={handleChange}/>
            I Agree to Terms
            </label>
            <button  onClick={ () => {
                if(checked){
                    alertData();
                }
            }}>
                Click me
            </button>
        </div>
    )
}
export default Agreement;