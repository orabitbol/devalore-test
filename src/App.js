import React, {useState}from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { nonNext } from './store/accordion'
import Home from './Screen/Home';
import PetInfo from './Screen/PetInfo';
import Agreement from './Screen/Agreement';

const App = () => {


 const accordion = useSelector((state) => state.accordion);
 

 console.log(accordion);
// "p-6 max-w-sm mx-autdo bg-white rounded-xl shadow-lg flex items-center space-x-4"
  return (
<div>
    
  <div id="accordion-open" data-accordion="open">
    <h2 id="accordion-open-heading-1">
      <button type="button" className="flex justify-between items-center p-5 w-full font-medium text-left text-gray-500 rounded-t-xl border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-1" aria-expanded="true" aria-controls="accordion-open-body-1">
        <span className="flex items-center"> Personal Info</span>
        <svg data-accordion-icon className="w-6 h-6 rotate-180 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
      </button>
    </h2>
    <div id="accordion-open-body-1" className={accordion.personalInfo== true ? '' : 'hidden'} aria-labelledby="accordion-open-heading-1">
      <div className="p-5 border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
        <Home/>
      </div>
    </div>
    <h2 id="accordion-open-heading-2">
      <button type="button" className="flex justify-between items-center p-5 w-full font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-2" aria-expanded="false" aria-controls="accordion-open-body-2">
        <span className="flex items-center">Per Info</span>
        <svg data-accordion-icon className="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
      </button>
    </h2>
    <div id="accordion-open-body-2" className={accordion.petInfo== true ? '' : 'hidden'} aria-labelledby="accordion-open-heading-2">
      <div className="p-5 border border-b-0 border-gray-200 dark:border-gray-700">
        <PetInfo/>
      </div>
    </div>
    <h2 id="accordion-open-heading-3">
      <button type="button" className="flex justify-between items-center p-5 w-full font-medium text-left text-gray-500 border border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-3" aria-expanded="false" aria-controls="accordion-open-body-3">
        <span className="flex items-center"> Agreemenet</span>
        <svg data-accordion-icon className="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
      </button>
    </h2>
    <div id="accordion-open-body-3" className={accordion.agreement== true ? '' : 'hidden'} aria-labelledby="accordion-open-heading-3">
      <div className="p-5 border border-t-0 border-gray-200 dark:border-gray-700">
        <Agreement/>
      </div>
    </div>
  </div>
</div>
  );
}

export default App;
      {/* <div>
        <div classNameName="text-xl font-medium text-primary">ChitChat</div>
        <p classNameName="text-slate-500">You dhave a new message!</p>
      </div>
      <div>
      <h1 classNameName='text-blue-600'> the count is: {counter}  </h1>
      <button classNameName='flex justify-center' onClick={() => dispatch(increment()) }> increment </button>
      <button classNameName='flex justify-center' onClick={() => dispatch(decrement())}> decrement  </button>
      <button classNameName='flex justify-center' onClick={() => dispatch(incrementByAmount(30))}> incrementByAmount by 30 </button>
      </div> 
      <Home/>*/}